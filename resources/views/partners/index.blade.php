@extends('main')

@section('content')

    <div class="static-content-wrapper">
        <div class="wrap-content">
            <div class="attendance-wrap">
                @include('common.errors')
                @include('flash::message')

                <div class="row">
                    <div class="col-md-12">
                        <div class="box-tab justified">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#partnar-tab1" data-toggle="tab">All Partners</a></li>
                                <li><a href="#partnar-tab2" data-toggle="tab">Add Partners</a></li>

                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active in" id="partnar-tab1">
                                    <div class="row mt30" id="project_details_div">
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                @if($partners->isEmpty())
                                                    <div class="well text-center">No Partners found.</div>
                                                @else
                                                    <table class="table table-bordered datatable editable-datatable responsive align-middle bordered">
                                                        <thead>
                                                        <tr class="bg-lightgrey">
                                                            <th>Name</th>
                                                            <th>Contact No</th>
                                                            <th>Alt Contact No</th>
                                                            <th>Email</th>
                                                            <th>Address</th>
                                                            <th>City</th>
                                                            <th>State</th>
                                                            <th>Country</th>
                                                            <th class="text-center">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($partners as $partner)
                                                            <tr>
                                                                <td> {{ $partner->first_name }} {{ $partner->middle_name }} {{ $partner->last_name }} </td>
                                                                <td> {{ $partner->contact_no }} </td>
                                                                <td> {{ $partner->alt_mobile_no }} </td>
                                                                <td> {{ $partner->email_id }} </td>
                                                                <td> {{ $partner->address    }} </td>
                                                                <td> {{ $partner->city    }} </td>
                                                                <td> {{ $partner->state    }} </td>
                                                                <td> {{ $partner->country    }} </td>

                                                                    <td class="text-center">
                                                                        <a href="{!! route('partners.edit', [$partner->id]) !!}" class="btn btn-icon-icon btn-sm btn-default infoicon btn-round">
                                                                            <i class="fa fa-lg fa-pencil"></i>
                                                                        </a>
                                                                    </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane " id="partnar-tab2">
                                    {!! Form::open(['route' => 'partners.store','class'=>'form-validation','role'=>'form']) !!}
                                    @include('partners.fields')
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        $("#filter_projects").on('click',function () {
            var project_name = $("#project_name").val();
            var project_type_id = $("#project_type_id").val();
            var project_status_id = $("#project_status_id").val();
            $("#project_details_div").empty();
            $.ajax({
                url: '/ajax/projects',
                data: "project_name=" + $('#project_name').val()+"&project_type_id=" + $('#project_type_id').val()+"&project_status_id=" + $('#project_status_id').val(),
                success: function (data) {
                    $('#project_details_div').append(data);
                    //alert(data);
                }
            });
        });
    </script>

@endsection
