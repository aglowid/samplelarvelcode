<?php

namespace App\Libraries\Repositories;


use App\Partner;
use Illuminate\Support\Facades\Schema;

class PartnerRepository
{

	/**
	 * Returns all Partners
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Partner::all();
	}

	public function search($input)
    {
        $query = Partner::query();

        $columns = Schema::getColumnListing('partners');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Partner into database
	 *
	 * @param array $input
	 *
	 * @return Partner
	 */
	public function store($input)
	{
		return Partner::create($input);
	}

	/**
	 * Find Partner by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Partner
	 */
	public function findPartnerById($id)
	{
		return Partner::find($id);
	}

	/**
	 * Updates Partner into database
	 *
	 * @param Partner $partner
	 * @param array $input
	 *
	 * @return Partner
	 */
	public function update($partner, $input)
	{
		$partner->fill($input);
		$partner->save();

		return $partner;
	}
}
