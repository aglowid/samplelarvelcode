<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    
	public $table = "partners";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
		"first_name",
		"middle_name",
		"last_name",
		"contact_no",
		"alt_mobile_no",
		"email_id",
		"address",
		"city",
		"state",
		"country",
		"user_id"
	];

	public static $rules = [
	    
	];
	public function user()
	{
			return $this->belongsTo('App\User','user_id','id');
	}

	public function partner_profit()
	{
		return $this->hasMany('App\PartnerProfit','partner_id');
	}
}
