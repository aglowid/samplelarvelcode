<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePartnerRequest;
use App\Partner;
use Illuminate\Http\Request;
use App\Libraries\Repositories\PartnerRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class PartnerController extends AppBaseController
{

	/** @var  PartnerRepository */
	private $partnerRepository;

	function __construct(PartnerRepository $partnerRepo)
	{
		$this->partnerRepository = $partnerRepo;
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the Partner.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->partnerRepository->search($input);

		$partners = $result[0];

		$attributes = $result[1];

		return view('partners.index')
		    ->with('partners', $partners)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Partner.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('partners.create');
	}

	/**
	 * Store a newly created Partner in storage.
	 *
	 * @param CreatePartnerRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePartnerRequest $request)
	{
        $input = $request->all();

		$partner = $this->partnerRepository->store($input);

		Flash::message('Partner saved successfully.');

		return redirect(route('partners.index'));
	}

	/**
	 * Display the specified Partner.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$partner = $this->partnerRepository->findPartnerById($id);

		if(empty($partner))
		{
			Flash::error('Partner not found');
			return redirect(route('partners.index'));
		}

		return view('partners.show')->with('partner', $partner);
	}

	/**
	 * Show the form for editing the specified Partner.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$partner = $this->partnerRepository->findPartnerById($id);

		if(empty($partner))
		{
			Flash::error('Partner not found');
			return redirect(route('partners.index'));
		}

		return view('partners.edit')->with('partner', $partner);
	}

	/**
	 * Update the specified Partner in storage.
	 *
	 * @param  int    $id
	 * @param CreatePartnerRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreatePartnerRequest $request)
	{
		$partner = $this->partnerRepository->findPartnerById($id);

		if(empty($partner))
		{
			Flash::error('Partner not found');
			return redirect(route('partners.index'));
		}

		$partner = $this->partnerRepository->update($partner, $request->all());

		Flash::message('Partner updated successfully.');
		return redirect(route('partners.index'));
	}

	/**
	 * Remove the specified Partner from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$partner = $this->partnerRepository->findPartnerById($id);

		if(empty($partner))
		{
			Flash::error('Partner not found');
			return redirect(route('partners.index'));
		}

		$partner->delete();

		Flash::message('Partner deleted successfully.');

		return redirect(route('partners.index'));
	}

}
